export default {
    getCryptoLisiting: state => state.cryptoLisiting,
    getNewCrypto: state => state.cryptoNew,
    getCryptoGainers: state => state.cryptoGainers,
    getLosersCrypto: state => state.cryptoLosers,
    getCoinInfo: state => state.coininfo,
    getHdata: state => state.hdata,
}
